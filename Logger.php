<?php


namespace Catmes\Log;


class Logger
{
    protected const DEFAULT_FILENAME = "catmes";
    protected const DEFAULT_DIRNAME = 'catmes';

    /* @var string 不带后缀的文件名 */
    protected $filename;
    protected $fileExt = ".log";

    /* @var string 不以斜杠结尾的日志文件所在的完整目录名 */
    protected $fileDir;

    protected $logType = 'debug';

    /**
     * 获取日志文件路径
     */
    protected function getFilepath(){
        return $this->fileDir . DIRECTORY_SEPARATOR . $this->filename . $this->fileExt;
    }

    /**
     * 初始化默认的日志文件名和文件夹名称
     */
    public function __construct($basePath, $filename='', $dirname='')
    {
        // 初始化日志文件名
        $filename = $filename=='' ? self::DEFAULT_FILENAME : $filename;
        // 日志文件名加日期后缀
        $dateStr = date('Y_m_d');
        $this->filename = $filename."_".$dateStr;

        // 初始化日志文件夹名
        $dirname = $dirname=='' ? self::DEFAULT_DIRNAME : $dirname;
        // 日志文件夹加月份后缀
        $monthStr = date('Y_m');
        $this->fileDir = $basePath . DIRECTORY_SEPARATOR. $dirname . $monthStr;
        if(!is_dir($this->fileDir)){
            mkdir($this->fileDir, 0777, true);
        }
    }

    public function add($msg, $logType='debug'){
        $this->logType = $logType;
        $this->write($msg);
    }

    protected function write($msg) {
        $filepath = $this->getFilepath();
        $lineBegin = date('Y-m-d H:i:s') . " [{$this->logType}] ";
        $string = $lineBegin . $msg . PHP_EOL . PHP_EOL; //"\r\n";
        $fileRes = fopen($filepath, "a+") or die("open file: " . $filepath . ' fail');
        //a+ 读写方式打开，将文件指针指向文件末尾。如果文件不存在则尝试创建之。 r+会直接替换第一行内容
        if (fwrite($fileRes, $string)) {
            fclose($fileRes);
        }
    }

}

简单的日志记录类

## 安装

```bash
$ composer require catmes/log
```

## 基本用法

```php
<?php

use Catmes\Log\Logger;
$logger = new Logger(__DIR__);
$logger->add('hello this is a simple log');
```
